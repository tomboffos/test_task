<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            'В ожидании',
            'В разработке',
            'На тестировании',
            'На проверке',
            'Выполнено',
        ];
        foreach ($arr as $item){
            Status::create([
                'name' => $item
            ]);
        }
    }
}
