<?php

namespace App\Models;

use App\Contracts\ProjectContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ProjectContract::FILLABLE;

    public function tasks()
    {
        $this->hasMany(Task::class);
    }
}
