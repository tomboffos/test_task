<?php


namespace App\Contracts;


interface ProjectContract
{
    const NAME = 'name';

    const FILLABLE = [
        self::NAME
    ];
}
