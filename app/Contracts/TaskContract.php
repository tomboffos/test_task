<?php


namespace App\Contracts;


interface TaskContract
{
    const NAME       = 'name';
    const USER_ID    = 'user_id';
    const STATUS_ID  = 'status_id';
    const PROJECT_ID = 'project_id';

    const FILLABLE = [
        self::NAME,
        self::USER_ID,
        self::STATUS_ID,
        self::PROJECT_ID
    ];
}
