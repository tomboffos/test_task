<?php


namespace App\Contracts;


interface StatusContract
{
    const NAME = 'name';

    const FILLABLE = [
        self::NAME
    ];
}
