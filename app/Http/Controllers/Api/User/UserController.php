<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function block(User $user)
    {
        $this->userRepository->blockUser($user);

        return response([
            'message' => 'User blocked successfully',
            'user' => new UserResource($user)
        ]);
    }

    public function delete(User $user)
    {
        $user->delete();

        return response([
            'message' => 'User deleted successfully'
        ]);
    }
}
