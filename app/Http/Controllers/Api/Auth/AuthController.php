<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\User\UserResource;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->userRepository->create($request);

        return response([
            'user' => new UserResource($user),
            'token' => $user->createToken(':o')->plainTextToken
        ], 201);
    }

    public function login(LoginRequest $request)
    {
        $user = $this->userRepository->login($request);
        if ($user) {
            return response([
                'user' => new UserResource($user),
                'token' => $user->createToken(':)')->plainTextToken
            ], 200);
        }else{
            return response([
                'message' => 'Incorrect email or password'
            ], 400);
        }
    }


}
