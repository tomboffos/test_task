<?php

namespace App\Http\Controllers\Api\Task;

use App\Contracts\TaskContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\CRUD\Task\TaskCreateRequest;
use App\Http\Requests\CRUD\Task\TaskUpdateRequest;
use App\Http\Resources\TaskResource;
use App\Models\Status;
use App\Models\Task;
use App\Repositories\TaskRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */

    private $userRepository, $taskRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->taskRepository = new TaskRepository();
    }


    public function index(Request $request)
    {
        return $this->taskRepository->search($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(TaskCreateRequest $request)
    {
        return new TaskResource(Task::create($request->safe()->merge([
            TaskContract::USER_ID => $request->user()->id,
            TaskContract::STATUS_ID => 1
        ])));
    }


    public function show(Task $task)
    {
        return new TaskResource($task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }


    public function update(TaskUpdateRequest $request, Task $task)
    {
        return new TaskResource($task->update($request->safe()));
    }


    public function destroy(Task $task)
    {
        $task->delete();

        return response([
            'message' => 'Task deleted successfully'
        ]);
    }

    public function showStatuses()
    {
        return Status::get();
    }
}
