<?php


namespace App\Repositories;


use App\Contracts\TaskContract;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserRepository implements UserRepositoryInterface
{
    public function create(RegisterRequest $request)
    {
        $user = User::create($request->validated());

        return $user;
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if (Auth::attempt($request->validated())) {
            return $user;
        }
    }


    public function blockUser(User $user)
    {
        $user->update([
            'blocked' => 1
        ]);

        return $user;
    }
}
