<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;

interface UserRepositoryInterface
{
    public function create(RegisterRequest $request);

    public function login(LoginRequest $request);
}
