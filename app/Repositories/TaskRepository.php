<?php


namespace App\Repositories;


use App\Contracts\TaskContract;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskRepository
{
    public function search(Request $request)
    {
        if ($request->user()->is_admin) {
            return TaskResource::collection(Task::where(function ($query) use ($request) {
                if ($request->has('search'))
                    $query->where(TaskContract::NAME, 'iLIKE', '%' . $request->search . '%');

                if ($request->has('status'))
                    $query->where(TaskContract::STATUS_ID, $request->status);
            })->orderBy('created_at', 'desc')->get());
        } else {
            return TaskResource::collection($request->user()->tasks($request));
        }
    }
}
