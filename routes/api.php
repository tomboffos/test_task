<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Project\ProjectController;
use App\Http\Controllers\Api\Task\TaskController;
use App\Http\Controllers\Api\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/auth')->group(function(){
    Route::post('/login',[AuthController::class,'login']);
    Route::post('/register',[AuthController::class,'register']);
});
Route::get('/statuses',[TaskController::class,'showStatuses']);

Route::middleware('auth:sanctum')->group(function (){
    Route::resources([
        'project' => ProjectController::class,
        'tasks'   => TaskController::class
    ]);


    Route::get('/bloc/{user}',[UserController::class,'block']);
    Route::get('/delete/{user}',[UserController::class,'delete']);
});
